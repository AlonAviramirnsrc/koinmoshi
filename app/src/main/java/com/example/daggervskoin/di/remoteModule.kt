package com.example.daggervskoin.di

import com.squareup.moshi.Moshi
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

val applicationModule = module {
    single { Moshi.Builder().build() }
    single { MoshiConverterFactory.create(get<Moshi>()) }
    single {
        Retrofit.Builder()
            .baseUrl("https://api.example.com")
            .addConverterFactory(get<MoshiConverterFactory>())
            .build()
    }

}