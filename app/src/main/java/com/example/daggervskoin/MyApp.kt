package com.example.daggervskoin

import android.app.Application
import com.squareup.moshi.Moshi
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.core.logger.Logger
import org.koin.core.logger.MESSAGE
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class MyApp : Application() {


    override fun onCreate() {
        super.onCreate()


        val androidModule = module {
            scope(named<KoinActivity>()) {
                scoped { MyPresenter() }
            }
        }

        val modules = listOf(androidModule)

        startKoin {
            androidLogger()

            logger(object : Logger() {
                override fun log(level: Level, msg: MESSAGE) {

                }

            })
            androidContext(this@MyApp)

            modules(modules)
        }

    }
}