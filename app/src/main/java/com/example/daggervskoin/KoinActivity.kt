package com.example.daggervskoin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import org.koin.android.ext.android.getKoin
import org.koin.core.qualifier.named

class KoinActivity : AppCompatActivity() {

    val activityScope = getKoin().createScope("activity", named<KoinActivity>())

    val myPresenter: MyPresenter by activityScope.inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_koin)
    }

    override fun onDestroy() {
        super.onDestroy()
        activityScope.close()
    }
}
