package com.example.daggervskoin

import com.example.daggervskoin.di.applicationModule
import org.junit.Test
import org.koin.dsl.koinApplication
import org.koin.test.check.checkModules

class KoinTest {

    @Test
    fun `Modules Test`() {
        koinApplication { modules(applicationModule) }.checkModules()
    }
}